#encoding: utf-8
module ApplicationHelper
  def postprocess_catalog_path s
	  s.sub! "<a href=\"/\">Домой</a>&nbsp;&raquo;&nbsp;</li>", "<a href=\"/\">Каталог</a>&nbsp;&raquo;&nbsp;</li>"
	  s.sub! "<li><a href=\"/products\">Товары</a>&nbsp;&raquo;&nbsp;</li>",""
	  raw(s)
  end
end
